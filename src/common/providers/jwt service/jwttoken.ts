import { Injectable, Res, UnauthorizedException } from '@nestjs/common';

import { JwtService } from '@nestjs/jwt';

import { Response } from 'express';

import { Login } from '../../../entity/login.entity';

@Injectable()
export class JwtToken {
  constructor(private jwtService: JwtService) {}

  async generateToken(data: Login) {
    const jwt = await this.jwtService.signAsync({ id: data.id });

    return jwt;
  }

  async verifyToken(token: string) {
    const data = await this.jwtService.verifyAsync(token);

    if (!data) {
      throw new UnauthorizedException();
    }

    return data;
  }

  async deleteToken(@Res({ passthrough: true }) response: Response) {
    response.clearCookie('jwt');

    return {
      message: 'logout'
    };
  }
}
