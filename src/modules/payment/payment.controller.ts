import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Req, Res } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { Payment } from 'src/entity/payment.entity';
import { PaymentService } from './payment.service';

/**
 *  payment controller includes handler for CRUD operations
 * @author:Charan
 */
@ApiTags('payment')
@Controller('payment')
export class PaymentController {
    /**
     * dependency injection
     * @param paymentService
     */
    constructor(private paymentService: PaymentService) { }
    /**
     * 
     * @param payment 
     * @returns payment
     * add the payment details
     */
    @Post()
    async addPayment(@Body() payment: Payment) {
        return this.paymentService.addPayment(payment);
    }
    /**
     *
     * @returns payment
     * get all the payment information
     *
     */
    @Get()
    getPayment(): Promise<Payment[]> {
        return this.paymentService.getPayment();
    }

    /**
     *
     * @param id
     * @returns payment
     * payment information according to payment id
     */
    @Get('/id/:id')
    getPaymentById(@Param('id') id: number): Promise<Payment> {
        return this.paymentService
            .getPaymentById(id)
            .then((result) => {
                if (result) {
                    return result;
                } else {
                    throw new HttpException('payment not found', HttpStatus.NOT_FOUND);
                }
            })
            .catch(() => {
                throw new HttpException('payment not found', HttpStatus.NOT_FOUND);
            });
    }

    /**
     *
     * @param id
     * @param payment
     * @returns payment
     * update the payment details
     * 
     */
    @Put()
    updatePayment(@Body('id') id: number, @Body() payment: Payment): Promise<Payment> {
        return this.paymentService.updatePayment(id, payment);
    }
    /**
     *
     * @param id
     * @returns payment
     * deleting the payment information based on paymentid
     */
    @Delete('/payment/:id')
    deletePaymentById(@Param('id') id: number) {
        return this.paymentService.deletePaymentById(id);
    }
}



