import { Payment } from './../../entity/payment.entity';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

/**
 * It will insert/update/delete/retrieve from database
 * PaymentService
 * @author:Charan
 */
@Injectable()
export class PaymentService {

    /**
     * dependency injection
     * @param paymentRepository 
     */
    constructor(
        @InjectRepository(Payment)
        private paymentRepository: Repository<Payment>
    ) { }

    /**
     *
     * @returns Payment
     * get all Payment in Payment entity
     */
    getPayment(): Promise<Payment[]> {
        return this.paymentRepository.find();
    }
    /**
     *
     * @param id :number
     * @returns Payment
     * Payment details by id
     */
    getPaymentById(id: number): Promise<Payment> {
        return this.paymentRepository.findOne(id);
    }

    /**
     *
     * @param Payment
     * @returns Payment
     * adding Payment details
     */
    async addPayment(payment: Payment) {

        const paymentdata = new Payment();
        paymentdata.userId = payment.userId;
        paymentdata.cardholderName = payment.cardholderName;
        paymentdata.cardNo = payment.cardNo;
        paymentdata.cvv = payment.cvv;
        paymentdata.expiryDate = payment.expiryDate;
        paymentdata.createdBy = payment.createdBy;
        paymentdata.createdDate = payment.createdDate;
        paymentdata.updatedBy = payment.updatedBy;
        paymentdata.updatedDate = payment.updatedDate;
        return this.paymentRepository.save(paymentdata);
    }

    /**
     * 
     * @param id 
     * @param user 
     * @returns user
  update the user information
    */
    async updatePayment(id: number, payment: Payment): Promise<Payment> {
        await this.updatePayment;
        return this.paymentRepository.findOne(id);
    }

    /**
     *
     * @param id
     * @returns  Payment
     * delete the Payment information by Paymentid
     */
    deletePaymentById(id: number) {
        return this.paymentRepository.delete({ id });
    }
}
