import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Food } from 'src/entity/food.entity';

/**
 * It will insert/update/delete/retrieve from database
 * FoodService
 * @author:Charan
 */
@Injectable()
export class FoodService {

    /**
     * dependency injection
     * @param foodRepository 
     */
    constructor(
        @InjectRepository(Food)
        private foodRepository: Repository<Food>
    ) { }

    /**
     *
     * @returns Food
     * get all Food in Food entity
     */
    getFood(): Promise<Food[]> {
        return this.foodRepository.find();
    }
    /**
     *
     * @param id :number
     * @returns Food
     * Food details by id
     */
    getFoodById(id: number): Promise<Food> {
        return this.foodRepository.findOne(id);
    }
    /**
     *
     * @param Food
     * @returns Food
     * adding Food details
     */
    async addFood(food: Food) {

        const fooddata = new Food();
        fooddata.Category = food.Category;
        fooddata.ItemName = food.ItemName;
        fooddata.Price = food.Price;
        fooddata.Quantity = food.Quantity;
        fooddata.Rating = food.Rating;
        fooddata.createdBy = food.createdBy;
        fooddata.createdDate = food.createdDate;
        fooddata.updatedBy = food.updatedBy;
        fooddata.updatedDate = food.updatedDate;
        return this.foodRepository.save(fooddata);
    }
    /**
    * 
    * @param id 
    * @param Food 
    * @returns Food
 update the Food information
   */
    updateFood(id: number, data: Food) {
        this.foodRepository.update({ id: id }, data)
        return 'Records successfully updated'
    }

    /**
     *
     * @param id
     * @returns  Food
     * delete the Food information by Foodid
     */
    deleteFoodById(id: number) {
        return this.foodRepository.delete({ id });
    }
}