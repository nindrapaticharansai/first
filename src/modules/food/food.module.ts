import { Food } from './../../entity/food.entity';
import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { FoodService } from './food.service';
import { FoodController } from './food.controller';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LoggerMiddleware } from 'src/common/middlewares/logger.middleware';

@Module({
  imports: [
    TypeOrmModule.forFeature([Food]),
  ],
  controllers: [FoodController],
  providers: [FoodService]
})
export class FoodModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware)
      .forRoutes({ path: 'food/*', method: RequestMethod.DELETE });
  }
}
