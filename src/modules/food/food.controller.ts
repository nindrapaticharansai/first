import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Req, Res } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { Food } from 'src/entity/food.entity';
import { FoodService } from './food.service';

/**
 *  food controller includes handler for CRUD operations
 * @author:Charan
 */
@ApiTags('food')
@Controller('food')
export class FoodController {
    /**
     * dependency injection
     * @param foodService
     */
    constructor(private foodService: FoodService) { }
    /**
     * 
     * @param food 
     * @returns food
     * add the food details
     */
    @Post()
    async addFood(@Body() food: Food) {
        return this.foodService.addFood(food);
    }
    /**
     *
     * @returns food
     * get all the food information
     *
     */
    @Get()
    getFood(): Promise<Food[]> {
        return this.foodService.getFood();
    }

    /**
     *
     * @param id
     * @returns food
     * food information according to food id
     */
    @Get('/id/:id')
    getFoodById(@Param('id') id: number): Promise<Food> {
        return this.foodService
            .getFoodById(id)
            .then((result) => {
                if (result) {
                    return result;
                } else {
                    throw new HttpException('food not found', HttpStatus.NOT_FOUND);
                }
            })
            .catch(() => {
                throw new HttpException('food not found', HttpStatus.NOT_FOUND);
            });
    }

    /**
     *
     * @param id
     * @param food
     * @returns food
     * update the food details
     * 
     */
    @Put('user/:id/:date')
    update(@Param('id') id: number, @Body() date: Food) {
        return this.foodService.updateFood(id, date);
    }
    /**
     *
     * @param id
     * @returns food
     * deleting the food information based on foodid
     */
    @Delete('/food/:id')
    deleteFoodById(@Param('id') id: number) {
        return this.foodService.deleteFoodById(id);
    }
}



