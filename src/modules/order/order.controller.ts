import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, ParseIntPipe, Post, Put } from '@nestjs/common';
import { ApiProperty, ApiTags } from '@nestjs/swagger';
import { Order } from 'src/entity/order.entity';
import { OrderService } from './order.service';

/**
 *  User controller includes handler for CRUD operations
 * @author:Charan
 */
@ApiTags('order')
@Controller('order')
export class OrderController {
    /**
     * dependency injection
     * @param userService
     */
    constructor(private orderService: OrderService) { }
    /**
     * 
     * @param order 
     * @returns order
     * add order details to database
     */
    @Post('/makeOrder')
    placeOrder(@Body() order: Order): Promise<Order> {
        return this.orderService.placeItemOrder(order);
    }



    /**
     * 
     * @param orderId 
     * @returns order
     * get order details by id
     */
    @Get('/:id')
    getById(@Param('id') orderId: number): Promise<Order> {
        return this.orderService
            .getorderById(orderId)
            .then((result) => {
                if (result) {
                    return result;
                } else {
                    throw new HttpException('user not found', HttpStatus.NOT_FOUND);
                }
            })
            .catch(() => {
                throw new HttpException('user not found', HttpStatus.NOT_FOUND);
            });
    }

    /**
     * 
     * @returns order
     * get all order details
     */
    @Get()
    getAllOrders(): Promise<Order[]> {
        return this.orderService.getAllOrders();
    }
    /**
     * 
     * @param id 
     * @param order 
     * @returns order
     * update order details
     */
    @Put()
    updateOrder(@Body('id') id: number, @Body() order: Order): Promise<Order> {
        return this.orderService.updateOrder(id, order);
    }

    /**
     *
     * @param id
     * @returns user
     * deleting the user information based on userid
     */
    @Delete('/order/:id')
    deleteOrderById(@Param('id') id: number) {
        return this.orderService.deleteOrderById(id);
    }
}