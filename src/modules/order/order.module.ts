import { orderDetails } from './../../entity/orderDetails.entity';
import { User } from 'src/entity/user.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from 'src/entity/order.entity';
import { OrderController } from './order.controller';
import { OrderService } from './order.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Order, User, orderDetails]),
  ],
  controllers: [OrderController],
  providers: [OrderService]
})
export class OrderModule { }
