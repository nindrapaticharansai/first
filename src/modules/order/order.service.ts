import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Order } from 'src/entity/order.entity';
import { orderDetails } from 'src/entity/orderDetails.entity';
import { User } from 'src/entity/user.entity';
import { Repository } from 'typeorm';

/**
 * It will insert/update/delete/retrieve from database
 * UserService
 * @author:Charan
 */
@Injectable()
export class OrderService {
    /**
     * dependency injection
     * @param orderRepository 
     * @param userRepository 
     * @param orderdetailRepository 
     */
    constructor(
        @InjectRepository(Order)
        private orderRepository: Repository<Order>,
        @InjectRepository(User)
        private userRepository: Repository<User>,
        @InjectRepository(orderDetails)
        private orderdetailRepository: Repository<orderDetails>,
    ) { }
    /**
     * 
     * @returns Order
     * get all Order details
     */

    async getAllOrders(): Promise<Order[]> {
        return this.orderRepository.find();
    }

    /**
     * 
     * @param order 
     * @returns Order
     * add order details
     */
    async placeItemOrder(order: Order): Promise<Order> {
        //  const userdata: User = await this.userRepository.findOne({PhoneNumber:PhoneNumber});

        return this.orderRepository.save(order);
    }
    /**
     * 
     * @param id Order
     * @returns Order
     * get order details by id
     */

    getorderById(id: number): Promise<Order> {
        return this.orderRepository.findOne({ id: id });
    }
    /**
     * 
     * @param id 
     * @param order 
     * @returns Order
     * update order details by id
     */

    async updateOrder(id: number, order: Order): Promise<Order> {
        await this.orderRepository.update(id, order);
        return this.orderRepository.findOne({ id });
    }
    /**
     *
     * @param id
     * @returns  order
     * delete the order information by orderid
     */
    deleteOrderById(id: number) {
        return this.orderRepository.delete({ id });
    }
}