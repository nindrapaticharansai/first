import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcryptjs';
import { Request, Response } from 'express';
import { User } from 'src/entity/user.entity';
import { Login } from 'src/entity/login.entity';
import { JwtToken } from 'src/common/providers/jwt service/jwttoken';
/**
 * It will insert/update/delete/retrieve from database
 * UserService
 * @author:Charan
 */
@Injectable()
export class UserService {
    /**
     * depency injection
     * @param userRepository
     * @param logRepository
     */
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
        @InjectRepository(Login)
        private logRepository: Repository<Login>,
        private jwttoken: JwtToken
    ) { }
    /**
     *
     * @param user
     * @returns User
     * adding user details
     */
    async addUser(user: User) {
        const pass = await bcrypt.hash(user.password, 10);
        const userdata = new User();
        const login = new Login();
        userdata.firstName = user.firstName;
        userdata.lastName = user.lastName;
        userdata.userId = user.userId;
        userdata.email = login.email = user.email;
        userdata.mobileNo = user.mobileNo;
        userdata.password = login.password = pass;
        userdata.Age = user.Age;
        userdata.Address = user.Address;
        userdata.State = user.State;
        userdata.pincode = user.pincode;
        userdata.createdBy = user.createdBy;
        userdata.createdDate = user.createdDate;
        userdata.updatedBy = user.updatedBy;
        userdata.updatedDate = user.updatedDate;
        userdata.login = login;

        return this.userRepository.save(userdata);
    }
    /**
     *
     * @returns user
     * get all users in user entity
     */
    getUser(): Promise<User[]> {
        return this.userRepository.find();
    }
    /**
     *
     * @param id :number
     * @returns user
     * user details by id
     */
    getUserById(id: number): Promise<User> {
        return this.userRepository.findOne(id);
    }

    /**
     *
     * @param data
     * @param response
     * @param req
     * @returns  login
     * add the login information
     */
    async login(data: Login, response: Response, req: Request) {
        const user = await this.logRepository.findOne({ email: data.email });
        console.log(user);
        if (!user) {
            throw new BadRequestException();
        }
        if (!(await bcrypt.compare(data.password, user.password))) {
            throw new BadRequestException();
        }
        const jwt = await this.jwttoken.generateToken(user);
        response.cookie('jwt', jwt, { httpOnly: true });
        console.log('login1');
        const cookie = req.cookies['jwt'];
        const c = await this.jwttoken.verifyToken(cookie);

        return c;
    }

    /**
     * 
     * @param id 
     * @param user 
     * @returns user
  update the user information
    */
    async updateUser(id: number, user: User): Promise<User> {
        await this.updateUser;
        return this.userRepository.findOne(id);
    }

    /**
     *
     * @param id
     * @returns  user
     * delete the user information by userid
     */
    deleteUserById(id: number) {
        //this.logger.log('get userby id is triggered!');
        return this.userRepository.delete({ id });
    }
}