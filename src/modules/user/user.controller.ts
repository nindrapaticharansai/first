import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Req, Res } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { UserService } from './user.service';
import { Request, Response } from 'express';
import { User } from 'src/entity/user.entity';
import { Login } from 'src/entity/login.entity';
/**
 *  User controller includes handler for CRUD operations
 * @author:Charan
 */
@ApiTags('user')
@Controller('user')
export class UserController {
    /**
     * dependency injection
     * @param userService
     */
    constructor(private userService: UserService) { }
    /**
     * 
     * @param user 
     * @returns user
     * add the user details
     */
    @Post()
    async addUser(@Body() user: User) {
        return this.userService.addUser(user);
    }
    /**
     *
     * @returns user
     * get all the user information
     *
     */
    @Get()
    getUser(): Promise<User[]> {
        return this.userService.getUser();
    }

    /**
     *
     * @param id
     * @returns user
     * user information according to user id
     */
    @Get('/id/:id')
    getUserById(@Param('id') id: number): Promise<User> {
        return this.userService
            .getUserById(id)
            .then((result) => {
                if (result) {
                    return result;
                } else {
                    throw new HttpException('user not found', HttpStatus.NOT_FOUND);
                }
            })
            .catch(() => {
                throw new HttpException('user not found', HttpStatus.NOT_FOUND);
            });
    }

    /**
     *
     * @param data
     * @param response
     * @param request
     * @returns login
     * add the login details
     */
    @Post('login')
    login(
        @Body() data: Login,
        @Res({ passthrough: true }) response: Response,
        @Req() request: Request
    ) {
        return this.userService.login(data, response, request);
    }

    /**
     *
     * @param id
     * @param user
     * @returns user
     * update the user details
     * 
     */
    @Put()
    updateUser(@Body('id') id: number, @Body() user: User): Promise<User> {
        return this.userService.updateUser(id, user);
    }
    /**
     *
     * @param id
     * @returns user
     * deleting the user information based on userid
     */
    @Delete('/user/:id')
    deleteUserById(@Param('id') id: number) {
        return this.userService.deleteUserById(id);
    }
}



