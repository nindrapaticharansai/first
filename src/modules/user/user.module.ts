import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LoggerMiddleware } from 'src/common/middlewares/logger.middleware';
import { JwtToken } from 'src/common/providers/jwt service/jwttoken';
import { Login } from 'src/entity/login.entity';
import { User } from 'src/entity/user.entity';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Login]),
    JwtModule.register({
      secret: 'secret',
      signOptions: { expiresIn: '1hr' }
    })
  ],
  controllers: [UserController],
  providers: [UserService, JwtToken]
})
export class UserModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware)
      .forRoutes({ path: 'user/*', method: RequestMethod.DELETE });
  }
}