import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import * as cookieParser from 'cookie-parser';
import { createDocument } from './config/swagger/swagger';


async function bootstrap() {

  const app = await NestFactory.create(AppModule);

  //app.useGlobalPipes(new ValidationPipes());
  //app.useGlobalFilters(new ValidationExceptionFilter());
  app.useGlobalPipes(
    new ValidationPipe({ transform: true })
  );

  app.use(cookieParser())
  app.enableCors({
    origin: 'http://localhost:8080',
    credentials: true
  })

  app.setGlobalPrefix("api");

  SwaggerModule.setup('api', app, createDocument(app))

  await app.listen(3005);

}
bootstrap();
