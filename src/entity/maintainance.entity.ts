import { PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { Column } from 'typeorm';

/**
 * creating entity for table creation
 */

export class Maintainance {



    @IsString()
    @ApiProperty()
    @Column()
    updatedBy: string;

    @IsString()
    @ApiProperty()
    @Column()
    updatedDate: string;

    @IsString()
    @ApiProperty()
    @Column()
    createdBy: string;

    @IsString()
    @ApiProperty()
    @Column()
    createdDate: string;
}