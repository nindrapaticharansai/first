import { Maintainance } from './maintainance.entity';
import { ApiProperty } from "@nestjs/swagger";
import { IsDate, IsInt, IsString } from "class-validator";
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm"
import { orderDetails } from "./orderDetails.entity";
import { User } from "./user.entity";

/**
 *  Casting typeORM to entity
 */
@Entity()
export class Order extends Maintainance {
    @PrimaryGeneratedColumn()
    id: number;


    /**
     * @type: string
     *  which accepts string data with char length of 3 to 15
     */
    @ApiProperty()
    @Column()
    @IsString()
    name: string;

    /**
     * @type:number
     *  which accepts integer data 
     */
    @ApiProperty()
    @Column()
    @IsInt()
    mobileNo: number;

    /**
     * @type: string
     *  which accepts string data with char length of 3 to 15
     */
    @ApiProperty()
    @Column()
    @IsString()
    address: string;


    /**
     * @type: string
     *  which accepts string data with char length of 3 to 15
     */
    @ApiProperty()
    @Column()
    @IsString()
    note: string;

    /**
     * @type: string
     *  which accepts string data with char length of 3 to 15
     */
    @ApiProperty()
    @Column()
    @IsString()
    status: string;

    /**
     * @type:number
     *  which accepts integer data 
     */
    @ApiProperty()
    @Column()
    @IsInt()
    totalPrice: number;

    /**
     * @type:date
     * which accepts date
     */
    @ApiProperty()
    @Column()
    @IsString()
    deliveredAt: string;

    @ManyToOne(() => User, (user) => user.order)
    @JoinColumn()
    user: User;
}
